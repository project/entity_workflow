<?php

/**
 * @file
 * Definition of entity_workflow_handler_relationship_cps.
 */

/**
 * Relationship handler to relate changeset and workflow state simultaneously.
 *
 * @ingroup views_relationship_handlers
 */
class entity_workflow_handler_relationship_cps extends views_handler_relationship  {

  /**
   * {@inheritdoc}
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['states'] = array('default' => array());
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  function options_form(&$form, &$form_state) {
    $states = cps_changeset_get_state_labels();
    $form['states'] = array(
      '#type' => 'checkboxes',
      '#title' => t('States'),
      '#options' => $states,
      '#default_value' => $this->options['states'],
      '#description' => t('Choose which states you wish to relate. Only entity revisions in changesets in the given states will be related.'),
      '#required' => TRUE,
    );
    
    parent::options_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  function query() {
    $this->ensure_my_table();
    $relationship = $this->relationship ? $this->relationship : $this->view->base_table;
    $base_table = $this->query->relationships[$relationship]['alias'];
    $table_data = views_fetch_data($this->query->relationships[$relationship]['base']);
    $entity_type = $this->definition['entity type'];

    // Add the cps_entity + cps_changeset subselect for the entity.
    // This subselect is required because a LEFT JOIN requires the filter
    // to be part of the join and but since what we're filtering on is
    // 2 tables away from our base, we need a subselect to collapse it
    // into 1 table.

    $cps_def = array(
      'table' => 'cps_entity',
      'field' => 'entity_id',
      'left_table' => $base_table,
      'left_field' => $table_data['table']['base']['field'],
      'type' => $this->options['required'] ? 'INNER' : 'LEFT',
    );

    $query = db_select('cps_entity', 'e');
    $query->addJoin('INNER', 'cps_changeset', 'c', 'c.changeset_id = e.changeset_id');
    $query->condition('c.status', array_filter($this->options['states']));
    $query->condition('e.entity_type', $entity_type);
    $query->fields('e');
    $query->fields('c', array('status', 'description', 'uid', 'changed'));
    $cps_def['table formula'] = $query;

    $cps_join = new views_join();
    $cps_join->definition = $cps_def;
    $cps_join->construct();
    $cps_join->adjusted = TRUE;

    // @todo Should this actually use add_relationship instead?
    $this->cps_alias = $this->query->add_table('cps_entity_select', $this->relationship, $cps_join);

    // Now add our workflow state table on the previous relationship.
    $alias = $relationship . '_workflow_entity';

    $workflow_def = array(
      'table' => 'entity_workflow_state',
      'field' => 'entity_id',
      'left_table' => $this->cps_alias,
      'left_field' => 'entity_id',
      'type' => $this->options['required'] ? 'INNER' : 'LEFT',
      // Note: This could fail if add_relationship changes the alias, but that
      // should only happen if our alias is already in use. That should not
      // ever happen because $relationship should be unique.
      'extra' => "$alias.entity_type = '$entity_type' AND $alias.changeset_id = $this->cps_alias.changeset_id",
    );

    $workflow_join = new views_join();
    $workflow_join->definition = $workflow_def;
    $workflow_join->construct();
    $workflow_join->adjusted = TRUE;

    $this->alias = $this->query->add_relationship($alias, $workflow_join, 'entity_workflow_state', $this->relationship);
  }
}
