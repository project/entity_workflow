<?php

/**
 * @file
 * Documentation for Entity Workflow hooks.
 */

/**
 * Set up workflows for entities.
 *
 * This function should only be used to set up worklow for an
 * entity you control. To modify an existing workflow set up by
 * another module, use hook_entity_workflows_alter().
 *
 * @return array
 */
function hook_entity_workflows() {
  return array(
    'my_entity' => array(
      // used for adding workflow menu item.
      'entity path' => 'my_entity/%my_entity',
      'default state' => 'state identifier',
      // If you want a log message written when an entity is added to CPS,
      // use the default transition. Set its valide states to something like array('none')
      // so it usable only by the system.
      'default transition' => 'transition identifier',
      'default transition log' => t('A default transition log message.'),
      // The default state callback, if used, will have $entity_type, $entity as arguments.
      'default state callback' => 'example_default_state_callback_function',
      'states' => array(
        'state_identifier' => array(
          'label' => t('State label'),
        )
      ),
      'transitions' => array(
        'transition_identifier' => array(
          'label' => t('Transition label'),
          // The transition callback should return the state to transition to.
          'callback' => 'example_callback_function',
          // $entity_type, $entity and $transition will always be
          // the first 3 argments; these are just additional.
          'callback arguments' => array('arg1', 'arg2'),
          'access callback' => 'example_access_function',
          // $entity_type, $entity and $transition will always be
          // the first 3 argments; these are just additional.
          'access arguments' => array('arg1', 'arg2'),
          // Prior to running access, check an optional list of valid
          // states to see if this transition should even be tested.
          'valid states' => array('state1', 'state2'),
          // If the transition should have a menu item, create it here.
          'menu' => array(
            'path' => 'entity/%entity/transition',
            'type' => MENU_LOCAL_ACTION,
            // As a menu item the title should be untranslated
            'title' => 'Transition label',
            'page callback' => 'entity_workflow_simple_transition_page',
            'page arguments' => array('my_entity', 1, 'transition_identifier'),
            'access callback' => 'entity_workflow_can_transition',
            'access arguments' => array('my_entity', 1, 'transition_identifier'),
          ),
          // entity_workflow_simple_transition_page() supports the following
          'transition log' => 'required', // TRUE for not required, FALSE for no log msg.
          'transition warning' => t('Are you sure you want to transition this entity?'),
          'weight' => 10, // for sorting transitions
        )
      ),
    )
  );
}

/**
 * @param array $workflows
 *
 * @see hook_entity_workflows().
 */
function hook_entity_workflows_alter(&$workflows) {
  $workflows['my_entity']['states']['new_state'] = array(
    'label' => t('New state'),
  );

  $workflows['my_entity']['transitions']['new_state'] = array(
    'label' => t('Transition to new_state'),
    'callback' => t('example_new_state_callback_function'),
  );
}

/**
 * Allows altering the workflow state label.
 *
 * @param string $label
 *   The current state label.
 * @param array $context
 *   An array of context data containing 'entity_type', 'entity', 'state',
 *   'state_info' and 'changeset'.
 */
function hook_entity_workflow_state_label_alter(&$label, array $context) {
  if ($context['entity_type'] == 'custom_entity') {
    $label = t('Custom label');
  }
}

/**
 * Allows altering the workflow state URI.
 *
 * @param array $uri
 *   A URI array containing 'path', 'options', 'entity_type', and 'entity' data.
 * @param array $context
 *   An array of context data containing 'entity_type', 'entity', 'state',
 *  'state_info', and 'changeset'.
 */
function hook_entity_workflow_state_uri_alter(array &$uri, array $context) {
  if ($context['entity_type'] == 'custom_entity') {
    $uri['options']['query']['custom_data'] = 'my-custom-data';
  }
}

/**
 * Allows giving access to the entity workflow tab.
 *
 * @param bool $access
 *   Whether or not to provide access. Will be FALSE by default unless changed
 *   by an alter.
 * @param string $entity_type
 *   The entity type.
 * @param object $entity
 *   The entity.
 */
function hook_entity_workflow_has_bulk_workflow_alter(&$access, $entity_type, $entity) {
  $access = TRUE;
}

/**
 * Return a list of entities that should be presented on the bulk workflow tab.
 *
 * @param string $entity_type
 *   The entity type.
 * @param object $entity
 *   The entity.
 *
 * @return array
 *   An array of entities. The key for each entity MUST be $entity_type . '--' .
 *   $entity_id.
 */
function hook_entity_workflow_bulk_workflow_entities($entity_type, $entity) {
  return array();
}

/**
 * Alter the target state of a transition before it is recorded.
 *
 * The transition is happening, all this can do is change the state. It can
 * also be used to react prior to the state being recorded, if necessary.
 *
 * @param string $new_state
 *   The state that the entity will be transitioned to.
 * @param array $context
 *   - entity_type: The entity type
 *   - entity: The entity object
 *   - transition: The name of the transition being executed.
 *   - previous_state: The state being transitioned away from.
 *   - log: The log message of the transition.
 */
function hook_entity_workflow_transition_alter(&$new_state, $context) {
  $new_state = 'different state than intended';
}

/**
 * React after a transition has been recorded.
 *
 * @param string $entity_type
 *   The entity type.
 * @param object $entity
 *   The entity being transitioned.
 * @param string $transition
 *   The name of the transition that was run.
 * @param string $previous_state
 *   The previous state of the entity.
 * @param string $new_state
 *   The new, now current state of the entity.
 * @param string $log
 *   The log message of the entity.
 */
function hook_entity_workflow_post_transition($entity_type, $entity, $transition, $previous_state, $new_state, $log) {
  // React to a transition after it has been recorded.
}
