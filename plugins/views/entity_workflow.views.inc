<?php

/**
 * Implements hook_views_data().
 */
function entity_workflow_views_data() {
  $data['entity_workflow_state']['table']['group'] = t('Entity workflow state');

  $data['entity_workflow_state']['state'] = array(
    'title' => t('Workflow state'),
    'help' => t('The current state of the entity.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'entity_workflow_handler_field_state',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'entity_workflow_handler_filter_state',
    ),
  );

  $data['entity_workflow_state']['changeset_id'] = array(
    'title' => t('Site version'),
    'relationship' => array(
      'label' => t('site version'),
      'base' => 'cps_changeset',
      'help' => t('Relate the site version to the entity workflow state.'),
    )
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function entity_workflow_views_data_alter(&$data) {
  $workflows = entity_workflow_get_workflows();
  foreach ($workflows as $entity_type => $workflow) {
    $entity_info = entity_get_info($entity_type);
    $table = $entity_info['base table'];

    $data[$table]['entity_workflow'] = array(
      'title' => t('Entity workflow state'),
      'help' => t('Relates the entity to the workflow state table through the CPS changeset.'),
      'relationship' => array(
        'handler' => 'entity_workflow_handler_relationship_cps',
        'label' => t('workflow'),
        'base' => 'entity_workflow_state',
        'entity type' => $entity_type,
      ),
    );
  }
}

