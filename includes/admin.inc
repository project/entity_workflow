<?php

/**
 * @file
 * Admin page callbacks for entity workflow module.
 */

/**
 * Page callback to display the entity workflow tab.
 *
 * @param string $entity_type
 *   The entity type.
 * @param object $entity
 *   The entity.
 * @return array
 *   A Drupal render array.
 */
function entity_workflow_workflow_tab($entity_type, $entity) {
  $workflow = entity_workflow_get_workflow($entity_type);
  // Display the workflow form
  $build['transition'] = drupal_get_form('entity_workflow_entity_status_form', $entity_type, $entity);

  // Display the workflow history
  $history = entity_workflow_get_history($entity_type, $entity);

  // Preload the accounts to reduce queries.
  $uids = array();
  foreach ($history as $item) {
    $uids[$item->uid] = $item->uid;
  }

  $accounts = user_load_multiple($uids);
  $items = array();
  foreach ($history as $item) {
    drupal_alter('cps_changeset_history_status', $status, $entity, $item);
    $transition = isset($workflow['transitions'][$item->transition]['label']) ? $workflow['transitions'][$item->transition]['label'] : t('Unknown transition');
    $state = isset($workflow['states'][$item->state]['label']) ? $workflow['states'][$item->state]['label'] : t('Unknown state');
    $items[] = array(
      $transition,
      $state,
      format_date($item->timestamp),
      format_username($accounts[$item->uid]),
      $item->log,
    );
  }

  $build['history'] = array(
    '#title' => t('History'),
    '#type' => 'item',
    '#theme' => 'table',
    '#header' => array(t('Transition'), t('New state'), t('Date'), t('Author'), t('Message')),
    '#rows' => $items,
    '#empty' => t('This entity has no history.'),
  );

  return $build;
}

/**
 * Form callback to show and change changeset status.
 */
function entity_workflow_entity_status_form($form, &$form_state, $entity_type, $entity) {
  $form_state['entity'] = $entity;
  $form_state['entity_type'] = $entity_type;

  $changeset_id = cps_get_current_changeset();
  $changeset = cps_changeset_load($changeset_id);
  // Do not display the form at all if the changeset is in a closed state.
  $states = cps_changeset_get_states();
  if ($states[$changeset->status]['type'] != 'open') {
    return $form;
  }

  $available_transitions = entity_workflow_get_transition_select($entity_type, $entity);

  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
    '#title' => t('Workflow'),
  );

  $workflow = entity_workflow_get_workflow($entity_type);
  $state = entity_workflow_get_entity_state($entity_type, $entity);

  // Create a list of transitions that use log messages so we can
  // apply #states and properly validate. Exclude unavailable transitions since
  // these will never need a log message.
  $log_transitions = [];
  foreach ($workflow['transitions'] as $transition_id => $transition) {
    if (isset($available_transitions[$transition_id]) && !empty($transition['transition log'])) {
      $log_transitions[] = $transition_id;
      if ($transition['transition log'] === 'required') {
        $log_required[$transition_id] = $transition_id;
      }
    }
  }

  $form['fieldset']['status'] = array(
    '#type' => 'item',
    '#title' => t('State'),
    '#markup' => entity_workflow_entity_state_label($entity_type, $entity, $state, $changeset),
  );

  $form['fieldset']['transition'] = array(
    '#type' => 'radios',
    '#options' => $available_transitions,
    '#required' => TRUE,
  );

  if (!empty($log_transitions)) {
    $form['fieldset']['log'] = array(
      '#type' => 'textarea',
      '#title' => t('Provide a log message'),
    );

    foreach ($log_transitions as $transition_id) {
      $form['fieldset']['log']['#states']['visible'][':input[name="transition"]'][] = array('value' => $transition_id);
    }

    if (!empty($log_required)) {
      foreach ($log_required as $transition_id) {
        $form['fieldset']['log']['#states']['required'][':input[name="transition"]'][] = array('value' => $transition_id);
      }
    }

  }

  $form['fieldset']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Change'),
    // Do not show the button if there are no transitions.
    '#access' => !empty($available_transitions),
  );

  $form['#attributes']['class'][] = 'entity-workflow-entity-status-form';

  return $form;
}

/**
 * Validate handler for the workflow status form.
 */
function entity_workflow_entity_status_form_validate($form, &$form_state) {
  $workflow = entity_workflow_get_workflow($form_state['entity_type'], $form_state['entity']);
  $transition = $form_state['values']['transition'];

  if (isset($workflow['transitions'][$transition]['transition log']) && $workflow['transitions'][$transition]['transition log'] === 'required' && empty($form_state['values']['log'])) {
    form_error($form['fieldset']['log'], t('Log field is required'));
  }

  // If they entered a log message when it was visible and then switched
  // to something where the log message was not visible, remove it so it
  // does not get recorded.
  if (empty($workflow['transitions'][$transition]['transition log'])) {
    $form_state['values']['log'] = '';
  }
}

/**
 * Submit handler for the workflow status form.
 */
function entity_workflow_entity_status_form_submit($form, &$form_state) {
  drupal_set_message(t('The workflow state has been changed'));
  entity_workflow_transition($form_state['entity_type'], $form_state['entity'], $form_state['values']['transition'], $form_state['values']['log']);
}

/**
 * Page callback to display the simple transition form.
 *
 * @param string $entity_type
 *   The entity type.
 * @param object $entity
 *   The entity.
 * @param string $transition
 *   The transition to attempt.
 *
 * @return array
 *   A Drupal renderable array.
 */
function entity_workflow_simple_transition_page($entity_type, $entity, $transition) {
  $workflow = entity_workflow_get_workflow($entity_type);
  $transition_info = $workflow['transitions'][$transition];

  $form_state = array(
      'entity_type' => $entity_type,
      'entity' => $entity,
      'transition' => $transition,
      'transition_info' => $transition_info,
    ) + form_state_defaults();

  $output = drupal_build_form('entity_workflow_simple_transition_form', $form_state);
  return $output;
}

/**
 * Form callback to display the simple transition form.
 */
function entity_workflow_simple_transition_form($form, &$form_state) {
  $transition = $form_state['transition'];
  $transition_info = $form_state['transition_info'];

  if (!empty($transition_info['transition warning'])) {
    $form['warning'] = array(
      '#markup' => '<div class="transition-warning ' . $transition . '-warning">' . $transition_info['transition warning'] . '</div>',
    );
  }

  if (!empty($transition_info['transition log'])) {
    $form['log'] = array(
      '#type' => 'textarea',
      '#title' => t('Provide a log message'),
      '#required' => $transition_info['transition log'] === 'required',
    );
  }
  else {
    $form['log'] = array(
      '#type' => 'value',
      '#value' => '',
    );
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => $transition_info['label'],
  );

  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#limit_validation_errors' => array(),
    '#submit' => array('entity_workflow_simple_transition_form_cancel'),
  );

  return $form;
}

/**
 * Submit handler to cancel the transition form.
 */
function entity_workflow_simple_transition_form_cancel($form, &$form_state) {
  $form_state['redirect'] = entity_uri($form_state['entity_type'], $form_state['redirect'] = $form_state['entity']);
}

/**
 * Submit handler for the simple transition form.
 */
function entity_workflow_simple_transition_form_submit($form, &$form_state) {
  entity_workflow_transition($form_state['entity_type'], $form_state['entity'], $form_state['transition'], $form_state['values']['log']);
  $form_state['redirect'] = entity_uri($form_state['entity_type'], $form_state['redirect'] = $form_state['entity']);
}

/**
 * Render the bulk operations tab to transition changeset states together.
 *
 * @param CPSChangeset $changeset
 * @return array
 *   A Drupal render array.
 */
function entity_workflow_bulk_entity_transition_changeset(CPSChangeset $changeset) {
  // Reconfigure changed entities into the format needed.
  $entities = array();

  foreach ($changeset->getChangedEntities() as $entity_type => $entity_type_entities) {
    foreach ($entity_type_entities as $entity_id => $entity) {
      $entities[$entity_type . '--' . $entity_id] = $entity;
    }
  }

  $form = drupal_get_form('entity_workflow_bulk_entity_transition_form', $entities, $changeset->changeset_id);
  return $form;
}

/**
 * Form callback for the bulk entity transition form.
 *
 * @param array $form
 *   The starting form array.
 * @param array $form_state
 *   The form state.
 * @param object[] $entities
 *   Each entity should have a key of $entity_type . '--' . $entity_id which
 *   is necessary to uniquely identify and utilize the entity.
 * @param string $changeset_id
 *   The changeset this is meant to be used for, or NULL for the current
 *   changeset. Note: If this is not for the current changeset, the
 *   form will be disabled and the user will be required to switch to
 *   this changeset to make changes.
 * @code
 *   [
 *     [
 *       'entity_type' => 'node',
 *       'entity' => $node,
 *     ],
 *     // ... etc ...
 *   ]
 * @endcode
 *
 * @return array
 *
 */
function entity_workflow_bulk_entity_transition_form($form, &$form_state, $entities, $changeset_id = NULL, $label_callback = 'entity_label') {
  if (!isset($changeset_id)) {
    $changeset_id = cps_get_current_changeset(TRUE);
  }

  $changeset = cps_changeset_load($changeset_id);

  // Build the sortable table header.
  $header = array(
    'title' => array('data' => t('Title')),
    'type' => array('data' => t('Type')),
    'state' => array('data' => t('State')),
  );

  // Because this can be used to handle transitions for multiple entity types,
  // and normally each entity type has its own workflow, we do a simple merge.
  // It is most likely that most entity types will have very similar workflows.
  // We assume any transition type with the same ID is the same transition,
  // and reduce to that. That does mean that if workflows have the same ID on
  // different transitions for different entity types, there will be weirdness.

  $transitions = array(
    '--create-draft--' => t('Add to site version'),
  );
  $transition_entities = array();
  $log_transitions = array();
  $log_required = array();

  $workflows = entity_workflow_get_workflows();
  foreach ($workflows as $entity_type => $workflow) {
    foreach ($workflow['transitions'] as $transition_id => $transition_info) {
      if (!isset($transitions[$transition_id])) {
        $transitions[$transition_id] = $transition_info['label'];
        if (!empty($transition_info['transition log'])) {
          $log_transitions[] = $transition_id;
          if ($transition_info['transition log'] === 'required') {
            $log_required[$transition_id] = $transition_id;
          }
          if (!empty($transition_info['transition log'])) {
            $log_used[$transition_id] = TRUE;
          }
        }
      }
    }
  }

  // Preload entity states.
  $all_entities = array();
  foreach ($entities as $entity_data => $entity) {
    list($entity_type) = explode('--', $entity_data, 2);
    $all_entities[$entity_type][] = $entity;
  }
  foreach ($all_entities as $entity_type => $entity_type_entities) {
    entity_workflow_load_entity_states($entity_type, $entity_type_entities);
  }

  // Build the rows.
  $options = array();
  $entity_info_all = entity_get_info();
  foreach ($entities as $entity_data => $entity) {
    list($entity_type, $entity_id) = explode('--', $entity_data, 2);
    list(,,$bundle) = entity_extract_ids($entity_type, $entity);
    if (entity_workflow_entity_is_in_workflow($entity_type, $entity, $changeset_id)) {
      $state = entity_workflow_get_entity_state($entity_type, $entity);
      $label = entity_workflow_entity_state_label($entity_type, $entity, $state, $changeset);
      // Figure out which transitions are valid and enable them for this entity.
      foreach (array_keys($transitions) as $transition) {
        if (entity_workflow_can_transition($entity_type, $entity, $transition, $changeset_id)) {
          $transition_entities[$transition][$entity_data] = $entity_data;
        }
      }
    }
    else {
      $label = t('Not in workflow');
      // Only the add to site version transition is valid for entities not
      // already in the site version.
      $transition_entities['--create-draft--'][$entity_data] = $entity_data;
    }

    $entity_type_string = $entity_info_all[$entity_type]['label'];

    if (count($entity_info_all[$entity_type]['bundles']) > 1) {
      $entity_type_string .= ': ' . $entity_info_all[$entity_type]['bundles'][$bundle]['label'];
    }

    $options[$entity_data] = array(
      'title' => $label_callback($entity_type, $entity),
      'type' => $entity_type_string,
      'state' => $label,
    );
  }

  // Remove any transitions that are not valid for our sent entities.
  // array_filter() only lets you filter on keys as of PHP 5.6 so we have
  // to do this the old fashioned way.
  foreach ($transitions as $transition_id => $transition_info) {
    if (!isset($transition_entities[$transition_id])) {
      unset($transitions[$transition_id]);
    }
  }

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => ['container-inline']),
  );

  $form['actions']['transition'] = array(
    '#type' => 'select',
    '#options' => $transitions,
    '#empty_option' => t('Choose a transition'),
    '#empty_value' => '',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Execute'),
  );

  if (!empty($log_transitions)) {
    $form['log'] = array(
      '#type' => 'textarea',
      '#title' => t('Provide a log message'),
    );

    foreach ($log_transitions as $transition_id) {
      $form['log']['#states']['visible'][':input[name="transition"]'][] = array('value' => $transition_id);
    }

    if (!empty($log_required)) {
      $form['#log_required'] = $log_required;
      foreach ($log_required as $transition_id) {
        $form['log']['#states']['required'][':input[name="transition"]'][] = array('value' => $transition_id);
      }
    }

    if (!empty($log_used)) {
      $form["#log_used"] = $log_used;
    }
  }

  $form['entities'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No entities available.'),
    // Disable the js select all because it does not work right with
    // the variable checkboxes.
    '#js_select' => FALSE,
  );

  $form['changeset_id'] = array(
    '#type' => 'hidden',
    '#value' => $changeset_id,
  );

  $form['#attached']['js'][] = ctools_attach_js('entity-workflow-bulk', 'entity_workflow');
  $form['#attached']['js'][] = array(
    'type' => 'setting',
    'data' => array(
      'EntityWorkflowBulkTransitions' => $transition_entities,
    ),
  );
  $form['#attached']['css'][] = ctools_attach_css('entity-workflow-bulk', 'entity_workflow');
  return $form;
}

/**
 * Validate handler for the workflow status form.
 */
function entity_workflow_bulk_entity_transition_form_validate($form, &$form_state) {
  $transition = $form_state['values']['transition'];
  if (!$transition) {
    form_error($form['actions']['transition'], t('You must select a transition.'));
    return;
  }

  if (!empty($form['#log_required'][$transition]) && empty($form_state['values']['log'])) {
    form_error($form['log'], t('Log field is required'));
  }

  // If they entered a log message when it was visible and then switched
  // to something where the log message was not visible, remove it so it
  // does not get recorded.
  if (empty($form['#log_used'][$transition])) {
    $form_state['values']['log'] = '';
  }

  if (!array_filter($form_state['values']['entities'])) {
    form_error($form['entities'], t('You must select one or more entities to transition.'));
  }
}

/**
 * Submit callback for the bulk transition form.
 * @param $form
 * @param $form_state
 */
function entity_workflow_bulk_entity_transition_form_submit($form, &$form_state) {
  cps_override_changeset($form_state['values']['changeset_id']);
  $transition = $form_state['values']['transition'];

  foreach (array_filter($form_state['values']['entities']) as $entity_data) {
    list($entity_type, $entity_id) = explode('--', $entity_data, 2);
    $entity = entity_load_single($entity_type, $entity_id);
    if ($transition == '--create-draft--') {
      entity_save($entity_type, $entity);
    }
    else {
      entity_workflow_transition($entity_type, $entity, $transition, $form_state['values']['log']);
    }
  }

  drupal_set_message(t('The transition has been executed.'));
  cps_override_changeset(NULL);
}

/**
 * Page callback to render the bulk workflow form for an entity.
 *
 * @param string $entity_type
 *   The entity type.
 * @param object $entity
 *   The entity.
 *
 * @return array
 *   A Drupal render array.
 */
function entity_workflow_bulk_workflow_tab($entity_type, $entity) {
  // Reconfigure changed entities into the format needed.
  $entities = module_invoke_all('entity_workflow_bulk_workflow_entities', $entity_type, $entity);

  // We didn't fetch all entities in access because it's potentially expensive.
  if (!$entities) {
    return MENU_NOT_FOUND;
  }

  list($entity_id) = entity_extract_ids($entity_type, $entity);

  // Add the current entity as the top one, regardless of what anyone else said.
  $entities = array($entity_type . '--' . $entity_id => $entity) + $entities;
  drupal_alter('entity_workflow_bulk_workflow_entities', $entities, $entity_type, $entity);

  // Access will already have failed if this isn't valid.
  $changeset_id = cps_get_current_changeset();
  $form = drupal_get_form('entity_workflow_bulk_entity_transition_form', $entities, $changeset_id);
  return $form;
}
