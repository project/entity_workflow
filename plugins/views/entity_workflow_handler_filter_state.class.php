<?php

/**
 * @file
 * Definition of views_handler_filter_node_type.
 */

/**
 * Filter by node type.
 *
 * @ingroup views_filter_handlers
 */
class entity_workflow_handler_filter_state extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('States');
      // Fetch the entity type from the relationship. Since this field
      // cannot appear on a base table there is always a relationship.
      $relationships = $this->view->display_handler->get_handlers('relationship');
      $entity_type = $relationships[$this->options['relationship']]->definition['entity type'];
      $workflow = entity_workflow_get_workflow($entity_type);

      $options = array();
      foreach ($workflow['states'] as $state => $info) {
        $options[$state] = $info['label'];
      }

      $this->value_options = $options;
    }
  }
}
