<?php

/**
 * @file
 * Definition of entity_workflow_handler_field_state.
 */

/**
 * Field handler to translate a node type into its readable form.
 *
 * @ingroup views_field_handlers
 */
class entity_workflow_handler_field_state extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  function render($values) {
    $value = $this->get_value($values);
    // Fetch the entity type from the relationship. Since this field
    // cannot appear on a base table there is always a relationship.
    $entity_type = $this->view->relationship[$this->options['relationship']]->definition['entity type'];
    $workflow = entity_workflow_get_workflow($entity_type);

    if ($value != NULL) {
      $value = isset($workflow['states'][$value]['label']) ? $workflow['states'][$value]['label'] : t('Unknown state');
    }

    return $value;
  }
}
