(function ($) {
  /**
   * Disable checkboxes for any entities that cannot be transitioned.
   */
  Drupal.behaviors.EntityWorkflowBulk = {
    attach: function (context, settings) {
      $('#entity-workflow-bulk-entity-transition-form table thead tr th:first-child')
        .once('EntityWorkflowBulkSelect')
        .each(function () {
          $(this).html('<input type="checkbox" name="select-all">');
          // Create a special select all checkbox. We use this one because core's select all checkbox cannot
          // handle dynamically enabled/disabled checkboxes.
          $('input[type="checkbox"]', this).change(function () {
            $('#entity-workflow-bulk-entity-transition-form td input[type="checkbox"]:enabled').attr('checked', this.checked);
          });
        });

      $('#entity-workflow-bulk-entity-transition-form select[name="transition"]')
        .once('EntityWorkflowBulkSelect')
        .change(function() {
        var transition = $(this).val();
        $('#entity-workflow-bulk-entity-transition-form table tbody td input[type="checkbox"]').each(function () {
          var entity_id = $(this).val();
          if (Drupal.settings.EntityWorkflowBulkTransitions[transition] && Drupal.settings.EntityWorkflowBulkTransitions[transition][entity_id]) {
            $(this).attr('disabled', false);
            $(this).parents('tr').addClass('transition-enabled').removeClass('transition-disabled');
          }
          else {
            $(this).attr('disabled', true).attr('checked', false);
            $(this).parents('tr').removeClass('transition-enabled').addClass('transition-disabled');
          }
        });
      }).trigger('change');

      // Make the entire td actually click the row.
      $('#entity-workflow-bulk-entity-transition-form tr')
        .once('EntityWorkflowBulkSelect')
        .bind('click', function (e) {
          // Ignore if they clicked on the actual checkbox.
          if (e.target.type == 'checkbox') {
            return;
          }

          // Toggle the checkbox.
          var $checkbox = $(this).find('input[type="checkbox"]');
          if ($checkbox.attr('disabled')) {
            return;
          }

          var val = $checkbox.attr('checked');

          // For some reason the attr gets set in the wrong order on our manual
          // trigger, so force the checkbox to be set correctly both before and
          // after so it happens the way it should.
          $checkbox.attr('checked', !val)
            .trigger('change');
        });

      $('#entity-workflow-bulk-entity-transition-form td input[type="checkbox"]')
        .once('EntityWorkflowBulkSelect')
        .change(function () {
          if (!this.checked) {
            $('#entity-workflow-bulk-entity-transition-form th input[type="checkbox"]').attr('checked', false);
          }
        });

    }
  }
})(jQuery);
